package br.com.snowmanlabs.test.andresilvaalves.category;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Category {

    @Id
    private UUID uuid = UUID.randomUUID();

    private String name;

}
