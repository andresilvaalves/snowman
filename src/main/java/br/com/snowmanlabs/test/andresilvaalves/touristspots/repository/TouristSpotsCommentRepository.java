package br.com.snowmanlabs.test.andresilvaalves.touristspots.repository;

import br.com.snowmanlabs.test.andresilvaalves.touristspots.model.TouristSpotsComment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "TouristSpotsComment", path = "tourist-spots-comment")
public interface TouristSpotsCommentRepository extends JpaRepository<TouristSpotsComment, UUID> {

}
