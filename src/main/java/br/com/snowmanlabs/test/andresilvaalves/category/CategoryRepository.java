package br.com.snowmanlabs.test.andresilvaalves.category;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "Category", path = "category")
public interface CategoryRepository extends JpaRepository<Category, UUID> {

}
