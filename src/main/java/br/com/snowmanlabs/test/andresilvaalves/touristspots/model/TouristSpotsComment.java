package br.com.snowmanlabs.test.andresilvaalves.touristspots.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tourist_spots_comment")
public class TouristSpotsComment {

    @Id
    private UUID uuid = UUID.randomUUID();

    @Column(precision = 2000)
    private String comments;

    @ManyToOne
    private TouristSpots touristSpots;

}
