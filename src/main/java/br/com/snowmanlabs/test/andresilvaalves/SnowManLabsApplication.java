package br.com.snowmanlabs.test.andresilvaalves;

import br.com.snowmanlabs.test.andresilvaalves.category.Category;
import br.com.snowmanlabs.test.andresilvaalves.category.CategoryRepository;
import br.com.snowmanlabs.test.andresilvaalves.touristspots.model.TouristSpots;
import br.com.snowmanlabs.test.andresilvaalves.touristspots.model.TouristSpotsComment;
import br.com.snowmanlabs.test.andresilvaalves.touristspots.model.TouristSpotsPicture;
import br.com.snowmanlabs.test.andresilvaalves.touristspots.repository.TouristSpotsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.geo.Point;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

import java.util.ArrayList;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@EnableSwagger2WebMvc
@Import({springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration.class})
@SpringBootApplication
public class SnowManLabsApplication {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private TouristSpotsRepository touristSpotsRepository;


    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .apiInfo(new ApiInfo("André Silva Alves", "Test Snow Man Labs", "0.0.1", "",
                                     new Contact("André Silva Alves", "", "andresilvaalves@gmail.com"), "", "", new ArrayList<>()));
    }

    @Bean
    public RepositoryRestConfigurer repositoryRestConfigurer() {
        return RepositoryRestConfigurer.withConfig(config -> {
            config.exposeIdsFor(Category.class);
            config.exposeIdsFor(TouristSpots.class);
            config.exposeIdsFor(TouristSpotsComment.class);
        });
    }

    public static void main(String[] args) {
        SpringApplication.run(SnowManLabsApplication.class, args);
    }

    @Component
    class DataWriter implements ApplicationRunner {

        @Override
        public void run(ApplicationArguments args) throws Exception {

            categoryRepository.saveAll(Stream.of("Park", "Museum", "Theater", "Monument").map(name -> {
                return Category.builder().uuid(UUID.randomUUID()).name(name).build();
            }).collect(Collectors.toList()));

            Category category = categoryRepository.findAll().stream().findFirst().orElseThrow();

            TouristSpots touristSpots = new TouristSpots();
            touristSpots.setName("Spots test ");
            touristSpots.setPictures(Stream.of(TouristSpotsPicture.builder().picture("base64:Image").build()).collect(Collectors.toList()));
            touristSpots.setCategory(category);
            touristSpots.setLocation(new Point(234, 233));
            touristSpots.setComment(Stream.of(TouristSpotsComment.builder().comments("nice place").build()).collect(Collectors.toList()));

            touristSpotsRepository.save(touristSpots);
        }
    }


}
