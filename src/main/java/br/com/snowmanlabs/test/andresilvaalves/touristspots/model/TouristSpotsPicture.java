package br.com.snowmanlabs.test.andresilvaalves.touristspots.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import java.util.UUID;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class TouristSpotsPicture {

    @Id
    @Builder.Default
    private UUID uuid = UUID.randomUUID();

    @Lob
    private String picture;

    @ManyToOne
    private TouristSpots touristSpots;
}
