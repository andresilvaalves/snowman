package br.com.snowmanlabs.test.andresilvaalves.touristspots.model;

import br.com.snowmanlabs.test.andresilvaalves.category.Category;
import lombok.Data;
import org.springframework.data.geo.Point;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Table(name = "tourist_spots")
public class TouristSpots {

    @Id
    private UUID uuid = UUID.randomUUID();

    private String name;

    @OneToMany(mappedBy = "touristSpots",  cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<TouristSpotsPicture> pictures;

    @ManyToOne
    private Category category;

    @Column(name = "location")
    private Point location;

    @OneToMany(mappedBy = "touristSpots",  cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<TouristSpotsComment> comment;

}
