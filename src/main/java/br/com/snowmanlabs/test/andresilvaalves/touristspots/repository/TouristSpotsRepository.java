package br.com.snowmanlabs.test.andresilvaalves.touristspots.repository;

import br.com.snowmanlabs.test.andresilvaalves.touristspots.model.TouristSpots;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "TouristSpots", path = "tourist-spots")
public interface TouristSpotsRepository extends JpaRepository<TouristSpots, UUID> {

    List<TouristSpots> findAllByNameIgnoreCaseContaining(String name);

    @Query(value = "SELECT t FROM TouristSpots t ")
    List<TouristSpots> findByLocationWithin();

}
