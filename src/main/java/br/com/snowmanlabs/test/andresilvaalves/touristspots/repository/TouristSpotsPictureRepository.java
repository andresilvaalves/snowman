package br.com.snowmanlabs.test.andresilvaalves.touristspots.repository;

import br.com.snowmanlabs.test.andresilvaalves.touristspots.model.TouristSpotsPicture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "TouristSpotsPicture", path = "tourist-spots-picture")
public interface TouristSpotsPictureRepository extends JpaRepository<TouristSpotsPicture, UUID> {
}
