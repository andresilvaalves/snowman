FROM openjdk:14-alpine
MAINTAINER André Silva Alves <andresilvaalves@gmail.com>

RUN mkdir -m 0755 -p /opt/app/ && mkdir -m 0755 -p /opt/app/log/

COPY target/*.jar /opt/app/application.jar

WORKDIR /opt/app/

EXPOSE 8080

ENTRYPOINT ["java", "-Djava.net.preferIPv4Stack=true", "-Djava.net.preferIPv4Addresses=true", "-Dspring.profiles.active=dev", "-jar","/opt/app/application.jar"]